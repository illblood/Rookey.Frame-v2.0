﻿/*----------------------------------------------------------------
        // Copyright (C) Rookey
        // 版权所有
        // 开发者：Rookey
        // Email：rookey@yeah.net
        // 
//----------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rookey.Frame.Base
{
    /// <summary>
    /// 用户登录事件参数
    /// </summary>
    public class EventUserArgs : EventArgs
    {
        private UserInfo _currUser;

        public UserInfo CurrUser
        {
            get { return _currUser; }
        }

        public EventUserArgs(UserInfo currUser)
        {
            this._currUser = currUser;
        }
    }
}
