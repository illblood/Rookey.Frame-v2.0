﻿//表单序列化扩展
//修改$("form:first").serializeArray(); 中某一名称的checkbox一个均未选择， 取不到name错误
$.fn.extend({
    "fixedSerializeArray": function () {
        var data = $(this).serializeArray();
        var $chks = $(this).find("#mainContent :checkbox:not(:checked)"); //取得所有未选中的checkbox
        if ($chks.length == 0) {
            return data;
        }
        $chks.each(function () {
            var chkName = $(this).attr("name");
            if (chkName) {
                data.push({ name: chkName, value: $(this).val() });
            }
        });
        return data;
    },
    "fixedSerializeArrayFix": function () {
        var $form = $(this);
        var $foreinInputs = $form.find("#mainContent input[foreignField='1']");
        if ($foreinInputs.length > 0) {
            $foreinInputs.each(function () {
                var foreinName = $(this).attr("id");
                if (foreinName) {
                    var obj = $form.find("input.textbox-value[name='" + foreinName + "']");
                    if (obj.length > 0) {
                        var v = $(this).attr('defaultValue');
                        if (v == undefined || v == null || v == '')
                            v = $(this).attr('v');
                        obj.attr('value', v);
                    }
                }
            });
        }
        var data = $form.serializeArray();
        var $chks = $form.find("#mainContent :checkbox"); //取得所有checkbox
        if ($chks.length > 0) {
            $chks.each(function () {
                if ($(this).attr('singleChk') == '1') { //对单选checkbox处理
                    var chkName = $(this).attr("name");
                    if (chkName) {
                        var index = -1;
                        for (var i = 0; i < data.length; i++) {
                            if (data[i].name == chkName) {
                                index = i;
                                break;
                            }
                        }
                        var obj = { name: chkName, value: $(this).val() == '1' };
                        if (index > -1)
                            data.splice(index, 1, obj)
                        else
                            data.push(obj);
                    }
                }
            });
        }
        //处理label控件
        var $labels = $form.find("#mainContent span[fieldSpan='0']");
        $labels.each(function (i, item) {
            var fieldName = $(this).attr("id");
            if (fieldName) {
                var obj = { name: fieldName, value: $(item).attr('value') };
                data.push(obj);
            }
        });
        return data;
    }
});
